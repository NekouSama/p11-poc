package openclassrooms.p11.application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import openclassrooms.p11.application.dto.HospitalInformationDTO;
import openclassrooms.p11.application.dto.PatientEmergencyRegistryDTO;
import openclassrooms.p11.domain.entity.Bed;
import openclassrooms.p11.domain.entity.Hospital;
import openclassrooms.p11.domain.entity.Patient;
import openclassrooms.p11.domain.repository.HospitalRepository;
import openclassrooms.p11.domain.repository.PatientRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Objects;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")

@AutoConfigureMockMvc
public class MedicalRecommendationControllerTest {

    @Autowired
    public MockMvc mockMvc;

    @Autowired
    public HospitalRepository hospitalRepository;

    @Autowired
    public PatientRepository patientRepository;

    @Test
    public void testAskRecommendation() throws Exception {
        PatientEmergencyRegistryDTO entry = new PatientEmergencyRegistryDTO();
        entry.firstName = "Loïc";
        entry.lastName = "RISTOURN";
        entry.birthDate = "2000-12-04";
        entry.longitude = 90;
        entry.latitude = 90;
        entry.medicalNeeds = "Cardiology";

        Gson gson = new GsonBuilder().create();
        String jsonEntry = gson.toJson(entry);

        MvcResult result = mockMvc.perform(
                post("/ask-recommendation")
                        .content(jsonEntry)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        HospitalInformationDTO dto = gson.fromJson(
                result.getResponse().getContentAsString(),
                HospitalInformationDTO.class
        );
        assertEquals("Fred Brooks", dto.name);
        assertEquals(90, dto.latitude);
        assertEquals(90, dto.longitude);
        assertEquals(0, dto.distanceInKmFromYourLocation);

        Hospital fredBrooksHospital = this.hospitalRepository.findAll().stream().filter((Hospital hospital) ->
                Objects.equals(hospital.getName(), "Fred Brooks")
            ).findFirst().get();
        Patient loicPatient =  this.patientRepository.findAll().stream().filter((Patient patient) ->
                patient.getLastName().equals("RISTOURN") && patient.getFirstName().equals("Loïc")
            ).findFirst().get();

        assertInstanceOf(Hospital.class, fredBrooksHospital);
        assertInstanceOf(Patient.class, loicPatient);
        assertTrue(fredBrooksHospital
                .getBeds()
                .stream()
                .anyMatch((Bed bed) ->
                        bed.hasEntry() &&
                        Objects.equals(bed.getEntry().getPatient().getFirstName(), loicPatient.getFirstName())));
    }
}
