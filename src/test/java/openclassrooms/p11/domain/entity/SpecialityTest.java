package openclassrooms.p11.domain.entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;

public class SpecialityTest {

    @Test
    void testReturnsFullSpecificationOfSpeciality() {
        Speciality speciality = new Speciality("Oral surgery");
        assertInstanceOf(Speciality.class, speciality);
        assertEquals("Oral surgery", speciality.getName());
    }

}
