package openclassrooms.p11.domain.entity;

import openclassrooms.p11.domain.value_object.Position;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class EmergencyEntryTest {

    @Mock
    private Patient mockedPatient;

    @Mock
    private Position mockedPosition;

    @Mock
    private Speciality mockedSpeciality;

    @Test
    void testReturnsFullSpecificationOfEmergencyEntry() {
        EmergencyEntry entry = new EmergencyEntry(mockedPatient, mockedPosition, mockedSpeciality);

        assertInstanceOf(EmergencyEntry.class, entry);
        assertEquals(mockedPosition, entry.getPosition());
        assertEquals(mockedSpeciality, entry.getMedicalNeeds());
        assertEquals(mockedPatient, entry.getPatient());
    }

}
