package openclassrooms.p11.domain.entity;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;

public class BedTest {

    @Mock
    private Hospital mockedHospital;

    @Test
    void testReturnsFullSpecificationOfBed() {
        Bed bed = new Bed(mockedHospital);
        assertInstanceOf(Bed.class, bed);
    }

}
