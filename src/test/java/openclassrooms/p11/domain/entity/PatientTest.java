package openclassrooms.p11.domain.entity;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {

    @Mock
    private LocalDate mockedDate;

    @Test
    void testReturnsFullSpecificationOfPatient() {
        Patient patient = new Patient("Andrew", "MITCH", mockedDate);

        assertEquals("Andrew", patient.getFirstName());
        assertEquals("MITCH", patient.getLastName());
        assertEquals(mockedDate, patient.getBirthDate());
        assertEquals(0, (long) patient.getEntries().size());
    }

}
