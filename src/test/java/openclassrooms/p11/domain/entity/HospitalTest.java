package openclassrooms.p11.domain.entity;

import openclassrooms.p11.domain.exception.NoBedAvailableException;
import openclassrooms.p11.domain.value_object.Position;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

public class HospitalTest {

    @Mock
    private Position mockedPosition;

    @Test
    void testReturnsFullSpecificationOfHospital() {
        Hospital hospital = new Hospital("Necker", mockedPosition);
        assertInstanceOf(Hospital.class, hospital);
        assertEquals("Necker", hospital.getName());
        assertEquals(0, hospital.getBeds().size());
        assertEquals(mockedPosition, hospital.getPosition());
        assertEquals(0, hospital.getSpecialities().size());
        assertThrows(NoBedAvailableException.class, hospital::getOneAvailableBed);
    }
}
