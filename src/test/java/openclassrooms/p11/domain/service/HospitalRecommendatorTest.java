package openclassrooms.p11.domain.service;

import openclassrooms.p11.domain.entity.*;
import openclassrooms.p11.domain.exception.NoHospitalAvailableException;
import openclassrooms.p11.domain.value_object.Position;
import openclassrooms.p11.domain.repository.HospitalRepository;
import openclassrooms.p11.domain.repository.SpecialityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class HospitalRecommendatorTest {

    @InjectMocks
    private HospitalRecommendator hospitalRecommendator;

    @Mock
    private SpecialityRepository specialityRepository;

    @Mock
    private HospitalRepository hospitalRepository;

    private List<Speciality> mockedSpecialities;

    @BeforeEach
    public void setUp() {
        mockedSpecialities = List.of(
                new Speciality("Immunology"),
                new Speciality("Cardiology"),
                new Speciality("Diagnostic neuropathology"),
                new Speciality("NoBedAvailableSpeciality")
        );

        List<Hospital> mockedHospitals = List.of(
            this.createHospital("Fred Brooks", new Position(90, 89), 2, List.of("Immunology", "Cardiology")),
            this.createHospital("Julia Crusher", new Position(90, 90), 0, List.of("Cardiology")),
            this.createHospital("Empty Hospital", new Position(40, 25), 0, List.of("NoBedAvailableSpeciality")),
            this.createHospital("Beverly Bashir", new Position(80, 80), 2, List.of("Immunology", "Diagnostic neuropathology"))
        );

        when(specialityRepository.findAll()).thenReturn(mockedSpecialities);
        when(hospitalRepository.findAll()).thenReturn(mockedHospitals);
    }


    public static Object[][] provideScenarioAndExpectedRecommendation() {
        return new Object[][] {
            { "Jean-Luc", "DUBON", "1976-02-23", "Cardiology", new Position(90,90), "Fred Brooks" },
            { "Derik", "McDouglas", "1989-04-04", "Immunology", new Position(80,80), "Beverly Bashir" }
        };
    }

    @ParameterizedTest
    @MethodSource("provideScenarioAndExpectedRecommendation")
    public void testFindRecommendedHospital(
            String firstName,
            String lastName,
            String birthDate,
            String medicalNeeds,
            Position position,
            String expectedRecommendedHospitalName
    ) {
        // GIVEN
        Patient patient0 = new Patient(firstName, lastName, LocalDate.parse(birthDate));

        List<Speciality> specialities = this.specialityRepository.findAll();
        Speciality speciality = specialities
                .stream()
                .filter(s -> s.getName().equals(medicalNeeds))
                .findFirst()
                .get();

        EmergencyEntry emergencyEntry = new EmergencyEntry(
                patient0,
                position,
                speciality
        );

        // WHEN
        Hospital recommendedHospital = this.hospitalRecommendator.getRecommendedHospital(emergencyEntry);

        // THEN
        assertEquals(expectedRecommendedHospitalName, recommendedHospital.getName());
    }

    @Test
    public void NoHospitalAvailable() {
        // GIVEN
        Patient patient0 = new Patient("Unlucky", "Dude", LocalDate.parse("2000-01-01"));

        List<Speciality> specialities = this.specialityRepository.findAll();
        Speciality speciality = specialities
                .stream()
                .filter(s -> s.getName().equals("NoBedAvailableSpeciality"))
                .findFirst()
                .get();

        EmergencyEntry emergencyEntry = new EmergencyEntry(
                patient0,
                new Position(0,0),
                speciality
        );

        // WHEN && THEN
        assertThrows(
                NoHospitalAvailableException.class,
                () -> this.hospitalRecommendator.getRecommendedHospital(emergencyEntry)
        );
    }

    private Hospital createHospital(String name, Position position, int bedCount, List<String> specialityNames) {
        Hospital hospital = new Hospital(name, position);
        for (int b = 0; b < bedCount; b++) {
            Bed bed = new Bed(hospital);
            hospital.addBed(bed);
        }

        List<Speciality> specialities = mockedSpecialities
                .stream()
                .filter(speciality -> specialityNames.contains(speciality.getName()))
                .toList();
        specialities.forEach(speciality -> speciality.addHospital(hospital));
        hospital.setSpecialities(specialities);

        return hospital;
    }
}
