package openclassrooms.p11.domain.value_object;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;

public class DistanceTest {

    @Test
    public void testCreateDistance() {
        // https://rosettacode.org/wiki/Haversine_formula#Java
        Position position1 = new Position(-86.67, 36.12);
        Position position2 = new Position(-118.40, 33.94);

        Distance distance = new Distance(position1, position2);
        assertInstanceOf(Distance.class, distance);

        // Value from the wiki formula
        assertEquals(2887.2599506071106, distance.getValueInKm());
    }

}
