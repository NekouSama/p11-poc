package openclassrooms.p11;

import openclassrooms.p11.application.controller.MedicalRecommendationController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.*;

@SpringBootTest()
class P11ApplicationTests {

	@Autowired
	private MedicalRecommendationController medicalRecommendationController;

	@Test
	void contextLoads() {
		assertThat(medicalRecommendationController).isNotNull();
	}

}
