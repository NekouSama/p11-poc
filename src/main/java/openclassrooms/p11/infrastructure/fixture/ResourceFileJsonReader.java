package openclassrooms.p11.infrastructure.fixture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
public class ResourceFileJsonReader {

    @Autowired
    ResourceLoader resourceLoader;

    public String read(String filename) throws IOException {
        StringBuilder responseStrBuilder = new StringBuilder();
        InputStream inputStream = resourceLoader.getResource("classpath:".concat(filename)).getInputStream();
        BufferedReader bR = new BufferedReader(  new InputStreamReader(inputStream));
        String lineRead = "";
        while((lineRead =  bR.readLine()) != null){
            responseStrBuilder.append(lineRead);
        }
        inputStream.close();

        return responseStrBuilder.toString();
    }

}
