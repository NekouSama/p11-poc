package openclassrooms.p11.infrastructure.fixture;

import openclassrooms.p11.domain.entity.Bed;
import openclassrooms.p11.domain.entity.Hospital;
import openclassrooms.p11.domain.entity.Speciality;
import openclassrooms.p11.domain.value_object.Position;
import openclassrooms.p11.domain.repository.HospitalRepository;
import openclassrooms.p11.domain.repository.SpecialityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.polak.springboot.datafixtures.DataFixture;
import ro.polak.springboot.datafixtures.DataFixtureSet;

import java.util.ArrayList;
import java.util.List;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
public class InitialHospitalFixture implements DataFixture {

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private SpecialityRepository specialityRepository;

    @Override
    public DataFixtureSet getSet() {
        return DataFixtureSet.DICTIONARY;
    }

    @Override
    public boolean canBeLoaded() {
        return hospitalRepository.findAll().size() == 0;
    }

    @Override
    public void load() {
        List<Speciality> specialities = this.specialityRepository.findAll();
        List<Hospital> hospitals = new ArrayList<>();

        Hospital hospital1 = new Hospital("Fred Brooks", new Position(90, 90));
        for (int b = 0; b < 2; b++) {
            Bed bed = new Bed(hospital1);
            hospital1.addBed(bed);
        }
        List<Speciality> specialitiesH1 = specialities
                .stream()
                .filter(speciality ->
                        speciality.getName().equals("Immunology")
                                || speciality.getName().equals("Cardiology")
                )
                .toList();
        hospital1.setSpecialities(specialitiesH1);
        hospitals.add(hospital1);

        Hospital hospital2 = new Hospital("Julia Crusher", new Position(90, 90));
        List<Speciality> specialitiesH2 = specialities
                .stream()
                .filter(speciality ->
                        speciality.getName().equals("Cardiology"))
                .toList();
        hospital2.setSpecialities(specialitiesH2);
        hospitals.add(hospital2);

        Hospital hospital3 = new Hospital("Beverly Bashir", new Position(80, 80));
        for (int b = 0; b < 5; b++) {
            Bed bed = new Bed(hospital3);
            hospital3.addBed(bed);
        }
        List<Speciality> specialitiesH3 = specialities
                .stream()
                .filter(speciality ->
                        speciality.getName().equals("Immunology")
                        || speciality.getName().equals("Diagnostic neuropathology")
                )
                .toList();
        hospital3.setSpecialities(specialitiesH3);
        hospitals.add(hospital3);

        this.hospitalRepository.saveAll(hospitals);
    }

}


