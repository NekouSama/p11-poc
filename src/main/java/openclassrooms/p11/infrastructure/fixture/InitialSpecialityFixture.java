package openclassrooms.p11.infrastructure.fixture;

import com.google.gson.Gson;
import openclassrooms.p11.domain.entity.Speciality;
import openclassrooms.p11.domain.repository.SpecialityRepository;
import org.json.JSONArray;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ro.polak.springboot.datafixtures.DataFixture;
import ro.polak.springboot.datafixtures.DataFixtureSet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class InitialSpecialityFixture implements DataFixture {

    @Autowired
    private SpecialityRepository specialityRepository;

    @Autowired
    private ResourceFileJsonReader resourceFileReader;

    @Override
    public DataFixtureSet getSet() {
        return DataFixtureSet.DICTIONARY;
    }

    @Override
    public boolean canBeLoaded() {
        return specialityRepository.findAll().size() == 0;
    }

    @Override
    public void load() {
        String jsonString = null;
        try {
            jsonString = this.resourceFileReader.read("fixture/speciality.json");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        JSONArray jsonArray = new JSONArray(new JSONTokener(jsonString));
        List<Speciality> specialities = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            Object jsonLine = jsonArray.get(i);
            Gson gson= new Gson();
            JsonSpecialityLine line = gson.fromJson(jsonLine.toString(), JsonSpecialityLine.class);

            specialities.add(new Speciality(line.speciality));
        }

        this.specialityRepository.saveAll(specialities);
    }
}


