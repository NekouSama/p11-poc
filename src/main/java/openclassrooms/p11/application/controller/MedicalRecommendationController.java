package openclassrooms.p11.application.controller;

import openclassrooms.p11.application.dto.HospitalInformationDTO;
import openclassrooms.p11.application.dto.PatientEmergencyRegistryDTO;
import openclassrooms.p11.application.exception.HttpBadRequestException;
import openclassrooms.p11.application.mapper.PatientEmergencyEntryRegistryMapper;
import openclassrooms.p11.domain.entity.EmergencyEntry;
import openclassrooms.p11.domain.entity.Hospital;
import openclassrooms.p11.domain.repository.EmergencyEntryRepository;
import openclassrooms.p11.domain.service.HospitalBedBooker;
import openclassrooms.p11.domain.service.HospitalRecommendator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MedicalRecommendationController {

    @Autowired
    PatientEmergencyEntryRegistryMapper entryRegistryMapper;

    @Autowired
    HospitalRecommendator hospitalRecommendator;

    @Autowired
    HospitalBedBooker hospitalBedBooker;

    @Autowired
    EmergencyEntryRepository emergencyEntryRepository;

    @PostMapping("/ask-recommendation")
    public HospitalInformationDTO askRecommendation(@RequestBody PatientEmergencyRegistryDTO dto) {
        try {
            EmergencyEntry entry = entryRegistryMapper.fromDto(dto);
            emergencyEntryRepository.save(entry);
            Hospital recommendedHospital = hospitalRecommendator.getRecommendedHospital(entry);
            hospitalBedBooker.book(entry, recommendedHospital);
            return new HospitalInformationDTO(recommendedHospital, entry);
        } catch (Exception e) {
            throw new HttpBadRequestException(e.getMessage());
        }
    }
}
