package openclassrooms.p11.application.mapper;

import openclassrooms.p11.application.dto.PatientEmergencyRegistryDTO;
import openclassrooms.p11.domain.entity.EmergencyEntry;
import openclassrooms.p11.domain.entity.Patient;
import openclassrooms.p11.domain.entity.Speciality;
import openclassrooms.p11.domain.repository.PatientRepository;
import openclassrooms.p11.domain.repository.SpecialityRepository;
import openclassrooms.p11.domain.value_object.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Optional;

@Component
public class PatientEmergencyEntryRegistryMapper {

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    SpecialityRepository specialityRepository;

    public EmergencyEntry fromDto(PatientEmergencyRegistryDTO dto) {

        Patient patient = this.getOrCreatePatient(dto);

        Optional<Speciality> medicalNeedsOrNull = specialityRepository.findOneByName(dto.medicalNeeds);
        if (medicalNeedsOrNull.isEmpty()) {
            throw new IllegalArgumentException("This speciality does not exist");
        }

        return new EmergencyEntry(
                patient,
                new Position(dto.longitude, dto.latitude),
                medicalNeedsOrNull.get()
        );
    }

    private Patient getOrCreatePatient(PatientEmergencyRegistryDTO dto) {
        Optional<Patient> patientOrNull = patientRepository.findOneByFirstNameAndLastNameAndBirthDate(
                dto.firstName,
                dto.lastName,
                LocalDate.parse("2013-12-14")
        );
        return patientOrNull.orElseGet(() -> new Patient(dto.firstName, dto.lastName, LocalDate.parse("2013-12-14")));
    }
}
