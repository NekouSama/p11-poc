package openclassrooms.p11.application.dto;

public class PatientEmergencyRegistryDTO {
    public String firstName;
    public String lastName;
    public String birthDate;
    public Integer longitude;
    public Integer latitude;
    public String medicalNeeds;
}
