package openclassrooms.p11.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class HttpBadRequestException extends RuntimeException {
    public HttpBadRequestException() {
        super();
    }
    public HttpBadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
    public HttpBadRequestException(String message) {
        super(message);
    }
    public HttpBadRequestException(Throwable cause) {
        super(cause);
    }
}