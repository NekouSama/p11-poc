package openclassrooms.p11.domain.service;

import openclassrooms.p11.domain.entity.Bed;
import openclassrooms.p11.domain.entity.EmergencyEntry;
import openclassrooms.p11.domain.entity.Hospital;
import openclassrooms.p11.domain.repository.BedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HospitalBedBooker {

    @Autowired
    private BedRepository bedRepository;

    public void book(EmergencyEntry entry, Hospital hospital) {
        Bed bed = hospital.getOneAvailableBed();
        bed.setEntry(entry);
        bedRepository.save(bed);
    }
}
