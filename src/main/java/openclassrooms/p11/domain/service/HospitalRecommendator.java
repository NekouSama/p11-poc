package openclassrooms.p11.domain.service;

import openclassrooms.p11.domain.entity.EmergencyEntry;
import openclassrooms.p11.domain.entity.Hospital;
import openclassrooms.p11.domain.exception.NoHospitalAvailableException;
import openclassrooms.p11.domain.value_object.Distance;
import openclassrooms.p11.domain.repository.HospitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class HospitalRecommendator {

    @Autowired
    private HospitalRepository hospitalRepository;

    public Hospital getRecommendedHospital(EmergencyEntry entry) throws NoHospitalAvailableException {
        List<Hospital> hospitals = this.hospitalRepository.findAll();

        Stream<Hospital> matchingSortedHospitals = hospitals.stream()
                .filter(hospital ->
                    hospital.hasAvailableBed() && hospital.containsSpeciality(entry.getMedicalNeeds()))
                .sorted((h1, h2) -> {
                    double kmFromH1 =  (new Distance(entry.getPosition(), h1.getPosition())).getValueInKm();
                    double kmFromH2 =  (new Distance(entry.getPosition(), h2.getPosition())).getValueInKm();
                    return  Double.compare(kmFromH1, kmFromH2);
                });

        Optional<Hospital> recommendHospital = matchingSortedHospitals.findFirst();
        if (recommendHospital.isEmpty()) {
            throw new NoHospitalAvailableException("There is no hospital with bed available that can take care of this medical needs");
        }
        return recommendHospital.get();
    }
}
