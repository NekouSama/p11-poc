package openclassrooms.p11.domain.exception;

public class NoHospitalAvailableException extends RuntimeException {
    public NoHospitalAvailableException(String message) {
        super(message);
    }
}
