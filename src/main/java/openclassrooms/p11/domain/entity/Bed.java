package openclassrooms.p11.domain.entity;

import jakarta.persistence.*;

@Entity
@Table
public class Bed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(targetEntity = EmergencyEntry.class)
    private EmergencyEntry entry;

    @ManyToOne
    private Hospital hospital;

    public Bed(Hospital hospital) {
        this.hospital = hospital;
    }

    private Bed() { }

    public boolean hasEntry() {
        return entry != null;
    }

    public EmergencyEntry getEntry() {
        return entry;
    }

    public void setEntry(EmergencyEntry entry) {
        this.entry = entry;
    }

    public boolean isReserved() {
        return entry != null;
    }

}
