package openclassrooms.p11.domain.value_object;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import org.hibernate.annotations.Immutable;

@Embeddable
@Immutable
public class Position {

    @Column(nullable = false)
    private double longitude;

    @Column(nullable = false)
    private double latitude;

    public Position(double longitude, double latitude) {
        if (longitude > 180 || longitude < -180) {
            throw new IllegalArgumentException("Longitude must be between -180 and 180");
        }
        this.longitude = longitude;

        if (latitude > 90 || latitude < -90) {
            throw new IllegalArgumentException("Latitude must be between -90 and 90");
        }
        this.latitude = latitude;
    }

    private Position() {};

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }
}
