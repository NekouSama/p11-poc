package openclassrooms.p11.domain.value_object;

public class Distance {

    private Position position1;

    private Position position2;

    public Distance(Position position1, Position position2) {
        this.position1 = position1;
        this.position2 = position2;
    }

    /**
     * Implement this algorithm https://rosettacode.org/wiki/Haversine_formula#Java
     */
    public double getValueInKm() {
        double dLat = Math.toRadians(position2.getLatitude() - position1.getLatitude());
        double dLon = Math.toRadians(position2.getLongitude() - position1.getLongitude());
        double lat1 = Math.toRadians(position1.getLatitude());
        double lat2 = Math.toRadians(position2.getLatitude());

        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return 6372.8 * c;
    }
}
