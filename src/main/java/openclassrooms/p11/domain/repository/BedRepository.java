package openclassrooms.p11.domain.repository;

import openclassrooms.p11.domain.entity.Bed;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BedRepository extends JpaRepository<Bed, Long> {
}