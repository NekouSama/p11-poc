package openclassrooms.p11.domain.repository;

import openclassrooms.p11.domain.entity.EmergencyEntry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmergencyEntryRepository extends JpaRepository<EmergencyEntry, Long> {
}