package openclassrooms.p11.domain.repository;

import openclassrooms.p11.domain.entity.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalRepository extends JpaRepository<Hospital, Long> {
}