package openclassrooms.p11.domain.repository;

import openclassrooms.p11.domain.entity.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SpecialityRepository extends JpaRepository<Speciality, Long> {
    Optional<Speciality> findOneByName(String name);
}