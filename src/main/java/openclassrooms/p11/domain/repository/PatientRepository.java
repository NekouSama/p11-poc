package openclassrooms.p11.domain.repository;

import openclassrooms.p11.domain.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface PatientRepository extends JpaRepository<Patient, Long> {

    Optional<Patient> findOneByFirstNameAndLastNameAndBirthDate(
            String firstName,
            String lastName,
            LocalDate birthDate
    );
}